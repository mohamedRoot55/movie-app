package com.root55.movieapp.use_case.main_screen

import com.root55.movieapp.chache.dao.TopMovieDao
import com.root55.movieapp.domain.models.TopMovie
import com.root55.movieapp.network.MoviesService
import com.root55.movieapp.utils.DataState
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class TopRatedMoviesUseCase
constructor(
    val topMovieDao: TopMovieDao,
    val moviesService: MoviesService
) {
    fun execute(
        isConnectedToInternet: Boolean,
        page: Int,
        apiKey: String
    ): Flow<DataState<List<TopMovie>>> = flow {

        try {
            emit(DataState.loading<List<TopMovie>>())


            if (isConnectedToInternet) {
                val topRatedMovies = makeNetworkRequest(apiKey = apiKey, page = page)
                topMovieDao.insertTopMovies(topRatedMovies)
            }
            val cachedTopRatedMovies = topMovieDao.getTopRatedMovies(page = page)
            emit(DataState.success(data = cachedTopRatedMovies))
        } catch (error: Exception) {
            emit(DataState.error<List<TopMovie>>("Unknown Error !!"))
        }


    }

    private suspend fun makeNetworkRequest(apiKey: String, page: Int): List<TopMovie> {
        return moviesService.getTopRatedMovies(apiKey = apiKey, page = page).result
    }

}