package com.root55.movieapp.use_case.movie_details

import android.util.Log
import com.root55.movieapp.chache.dao.MovieDao
import com.root55.movieapp.domain.models.Movie
import com.root55.movieapp.network.MoviesService
import com.root55.movieapp.utils.Constants.TAG
import com.root55.movieapp.utils.DataState
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class MovieDetailsUseCase
constructor(
    private val movieDao: MovieDao,
    private val moviesService: MoviesService
) {

    fun execute(
        apiKey: String,
        isConnectedToInternet: Boolean,
        movieId: Int
    ): Flow<DataState<Movie>> = flow {
        Log.d(TAG, "execute: MovieID $movieId")
      try{
          emit(DataState.loading())


          if(isConnectedToInternet){
              val movie = makeNetworkRequest(apiKey = apiKey , movieId = movieId)

               movieDao.insertMovie(movie = movie)
          }

          val cachedMovie = movieDao.getMovieById(id = movieId)
          emit(DataState.success(data = cachedMovie))
      }catch (e : Exception){
          Log.d(TAG, "execute: ERROR! ${e.printStackTrace()}")
          emit(DataState.error<Movie>(error = "Unknown Error!!"))
      }

    }

    private suspend fun makeNetworkRequest(apiKey: String , movieId: Int): Movie {
        return moviesService.getMovieById(apiKey = apiKey , movieId = movieId)
    }
}