package com.root55.movieapp.use_case.movie_details

import android.util.Log
import com.root55.movieapp.domain.models.Trailer
import com.root55.movieapp.network.MoviesService
import com.root55.movieapp.utils.Constants.TAG
import com.root55.movieapp.utils.DataState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class MovieTrailerUseCase constructor(
    private val moviesService: MoviesService
) {
    fun execute(
        isConnectedToInternet: Boolean,
        apiKey: String,
        movieId: Int
    ): Flow<DataState<List<Trailer>>> = flow {
        Log.d(TAG, "execute: movieId $movieId ")
        emit(DataState.loading())

        if (isConnectedToInternet) {
            val trailers = moviesService.getMovieTrailer(movieId = movieId, apiKey = apiKey).trailers

            Log.d(TAG, "execute: trailers $trailers ")
            emit(DataState.success(data = trailers))
        } else {
            emit(DataState.error<List<Trailer>>(error = "No Internet Connection"))
        }
    }
}