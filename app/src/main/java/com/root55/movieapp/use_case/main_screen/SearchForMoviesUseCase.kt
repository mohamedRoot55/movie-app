package com.root55.movieapp.use_case.main_screen

import android.util.Log
import com.root55.movieapp.chache.dao.TopMovieDao
import com.root55.movieapp.domain.models.TopMovie
import com.root55.movieapp.utils.Constants.TAG
import com.root55.movieapp.utils.DataState
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class SearchForMoviesUseCase
constructor(
    private val topMovieDao: TopMovieDao
) {
    fun execute( query: String, ): Flow<DataState<List<TopMovie>>> = flow {

        Log.d(TAG, "execute: query $query")
        try {
            emit(DataState.loading())



            val cachedMovies = topMovieDao.searchForMovies(query = query)

            Log.d(TAG, "execute: ${cachedMovies.first()}")

            emit(DataState.success(data = cachedMovies))

        } catch (error: Exception) {
            emit(DataState.error<List<TopMovie>>(error = "UnKnown Error!!"))
        }


    }

}