package com.root55.movieapp.use_case.main_screen

import com.root55.movieapp.chache.dao.TopMovieDao
import com.root55.movieapp.domain.models.TopMovie
import com.root55.movieapp.network.MoviesService
import com.root55.movieapp.utils.Constants.PAGE_SIZE
import com.root55.movieapp.utils.DataState

import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class PopularMoviesUseCase
constructor(
    private val topMovieDao: TopMovieDao,
    private val moviesService: MoviesService,
) {

    fun execute(
        apiKey: String,
        isConnectedToInternet: Boolean,
        page: Int
    ): Flow<DataState<List<TopMovie>>> = flow {
        try {
            emit(DataState.loading<List<TopMovie>>())


            if (isConnectedToInternet) {
                val topMovies: List<TopMovie> = makeNetWorkRequest(apiKey = apiKey)
                topMovieDao.insertTopMovies(topMovies)
            }

            val cachedTopMovies: List<TopMovie> =
                topMovieDao.getTopMovies(page = page, pageSize = PAGE_SIZE)

            emit(DataState.success(data = cachedTopMovies))

        } catch (error: Exception) {
            emit(DataState.error<List<TopMovie>>(error = "UnKnown Error!! \n ${error.printStackTrace()}"))
        }
    }

    private suspend fun makeNetWorkRequest(apiKey: String): List<TopMovie> {
        return moviesService.getPopularMovies(
            apiKey = apiKey,
            page = PAGE_SIZE
        ).result
    }

}