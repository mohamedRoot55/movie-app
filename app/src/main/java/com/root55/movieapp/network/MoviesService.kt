package com.root55.movieapp.network

import com.root55.movieapp.domain.models.Movie
import com.root55.movieapp.network.response.TrailerResponse
import com.root55.movieapp.network.response.TopMovieResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MoviesService {

    @GET("movie/popular")
   suspend fun getPopularMovies(
        @Query("api_key") apiKey : String,
        @Query("page") page : Int,
    ) : TopMovieResponse

    @GET("movie/top_rated")
    suspend fun getTopRatedMovies(
        @Query("api_key") apiKey : String,
        @Query("page") page : Int,
    ) : TopMovieResponse

    @GET("movie/{movie_id}")
   suspend fun getMovieById(
        @Path("movie_id") movieId : Int ,
        @Query("api_key") apiKey : String

    ) : Movie

   @GET("movie/{movie_id}/videos")
   suspend fun getMovieTrailer(
        @Path("movie_id") movieId : Int ,
        @Query("api_key") apiKey : String
   ) : TrailerResponse


}