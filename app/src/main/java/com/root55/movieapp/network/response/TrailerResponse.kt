package com.root55.movieapp.network.response

import com.google.gson.annotations.SerializedName
import com.root55.movieapp.domain.models.Trailer

data class TrailerResponse(
    @SerializedName("id")
    val id: Int,
    @SerializedName("results")
    val trailers: List<Trailer>
)