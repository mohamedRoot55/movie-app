package com.root55.movieapp.network.response

import com.root55.movieapp.domain.models.TopMovie
import com.google.gson.annotations.SerializedName

data class TopMovieResponse(
    @SerializedName("page")
    val page: Int,
    @SerializedName("results")
    val result: List<TopMovie>
)