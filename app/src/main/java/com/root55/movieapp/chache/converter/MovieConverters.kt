package com.root55.movieapp.chache.converter

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class MovieConverters {
    private val gson = Gson()
    private var genreIdsType = object : TypeToken<List<Int>>(){}.type

    @TypeConverter
    fun fromGenresIdsToGson(genresIds : List<Int>) : String?{
        return gson.toJson(genresIds  , genreIdsType)
    }

    @TypeConverter
    fun fromGsonToGenresIds(genresIds : String) : List<Int>?{
        return gson.fromJson(genresIds  , genreIdsType)
    }
}