package com.root55.movieapp.chache.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.root55.movieapp.chache.converter.MovieConverters
import com.root55.movieapp.chache.converter.TopMovieConverters
import com.root55.movieapp.chache.dao.MovieDao
import com.root55.movieapp.chache.dao.TopMovieDao
import com.root55.movieapp.domain.models.Movie
import com.root55.movieapp.domain.models.TopMovie

@Database(entities = [Movie::class, TopMovie::class], version = 1 , exportSchema = false)
@TypeConverters(TopMovieConverters::class , MovieConverters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun movieDao(): MovieDao
    abstract fun topMovieDao(): TopMovieDao

    companion object{
        const val APP_DATABASE_NAME = "movies_dp"
    }
}