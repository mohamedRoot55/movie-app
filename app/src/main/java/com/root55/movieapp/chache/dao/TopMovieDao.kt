package com.root55.movieapp.chache.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.root55.movieapp.domain.models.TopMovie

@Dao
interface TopMovieDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertTopMovie(topMovie: TopMovie): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertTopMovies(topMovies: List<TopMovie>): List<Long>

    @Query("delete from top_movie")
    suspend fun deleteAllTopMovies()

    @Query("select * from top_movie limit :pageSize offset ((:page -1 ) * :pageSize )")
    suspend fun getTopMovies(
        page: Int,
        pageSize: Int = 30
    ): List<TopMovie>

    @Query("select * from top_movie where vote_average >= 8 limit :pageSize offset ((:page -1) * :pageSize)")
    suspend fun getTopRatedMovies(page: Int , pageSize: Int = 30) : List<TopMovie>

    @Query("select * from top_movie where title like '%'||:query||'%' or original_title like '%'||:query||'%'")
    suspend fun searchForMovies(query : String) : List<TopMovie>

}