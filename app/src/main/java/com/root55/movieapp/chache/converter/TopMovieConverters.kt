package com.root55.movieapp.chache.converter;

import androidx.room.TypeConverter;
import com.root55.movieapp.domain.models.Genre
import com.root55.movieapp.domain.models.ProductionCompany
import com.root55.movieapp.domain.models.ProductionCountry
import com.root55.movieapp.domain.models.SpokenLanguage
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken


class TopMovieConverters {
    private val gson = Gson()
    private var productionCompanyType = object : TypeToken<List<ProductionCompany>>(){}.type
    private var productionCountryType = object : TypeToken<List<ProductionCountry>>(){}.type
    private var spokenLanguageType = object : TypeToken<List<SpokenLanguage>>(){}.type
    private var genreType = object : TypeToken<List<Genre>>(){}.type

    @TypeConverter
    fun fromProductionCompanyListToGson(productionCompanyList: List<ProductionCompany>): String? {
        return gson.toJson(productionCompanyList, productionCompanyType)
    }

    @TypeConverter
    fun toProductionCompanyList(productionCompanyList : String) :  List<ProductionCompany> {
        return gson.fromJson(productionCompanyList , productionCompanyType)
    }

    @TypeConverter
    fun fromProductionCountryListToGson(productionCountryList: List<ProductionCountry>): String? {
        return gson.toJson(productionCountryList, productionCountryType)
    }

    @TypeConverter
    fun toProductionCountryList(productionCountryList : String) :  List<ProductionCountry> {
        return gson.fromJson(productionCountryList , productionCountryType)
    }

    @TypeConverter
    fun fromSpokenLanguageListToGson(spokenLanguages: List<SpokenLanguage>): String? {
        return gson.toJson(spokenLanguages, spokenLanguageType)
    }

    @TypeConverter
    fun toSpokenLanguageList(spokenLanguages : String) :  List<SpokenLanguage> {
        return gson.fromJson(spokenLanguages , spokenLanguageType)
    }

    @TypeConverter
    fun fromGenreListToGson(genres: List<Genre>): String? {
        return gson.toJson(genres, genreType)
    }

    @TypeConverter
    fun toGenreList(genres : String) :  List<Genre> {
        return gson.fromJson(genres , genreType)
    }

}
