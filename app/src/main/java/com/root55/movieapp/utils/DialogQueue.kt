package com.root55.movieapp.utils

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import com.root55.movieapp.persentation.component.GeneralDialogInfo
import com.root55.movieapp.persentation.component.PositiveAction
  import java.util.*

class DialogQueue {

    val queue: MutableState<Queue<GeneralDialogInfo>> = mutableStateOf(LinkedList())

    private fun removeQueueHead() {
        if (queue.value.isNotEmpty()) {
            var update = queue.value
            update.remove() //remove first message
            queue.value = ArrayDeque() // force recompose(bug)
            queue.value = update
        }
    }
    fun appendErrorMessage(title: String, desc: String) {
        queue.value.offer(
            GeneralDialogInfo.Builder()
                .title(title)
                .description(desc).onDismiss {
                    removeQueueHead()
                }
                .positive(
                    PositiveAction(
                        onClick = {
                            removeQueueHead()
                        },
                        title = "Ok"
                    )
                ).build()
        )
    }
}