package com.root55.movieapp.utils

data class DataState<out T> (
    val data : T? = null ,
    val loading : Boolean = false ,
    val error : String ? = null
){
    companion object{
        fun <T> success(
            data : T?
        ) : DataState<T> {
            return  DataState(data = data)
        }

        fun <T>error (
            error: String?
        ): DataState<T> {
            return DataState(error = error)
        }

        fun <T>loading() : DataState<T> {
            return DataState(loading = true)
        }
    }
}