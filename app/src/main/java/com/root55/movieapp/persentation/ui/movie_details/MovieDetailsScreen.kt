package com.root55.movieapp.persentation.ui.movie_details

import androidx.compose.material.Scaffold
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.Composable
import com.root55.movieapp.persentation.theme.Theme.appTheme

@Composable
fun MovieDetailsScreen(
    isConnectedToInternet: Boolean,
    isDarkTheme: Boolean,
    movieId: Int?,
    viewModel: MovieDetailsViewModel
) {
    if (movieId == null) {
        //todo("show invalid movie)
    } else {
        val onLoad = viewModel.onLoad.value
        if (!onLoad) {
            viewModel.onLoad.value = true
            viewModel.onTriggerEvent(
                MovieDetailsStateEvent.GetMovieDetails(movieId = movieId)
            )

            viewModel.onTriggerEvent(
                MovieDetailsStateEvent.GetMovieTrailers(movieId = movieId)
            )
        }

        val loading = viewModel.loading
        val movie = viewModel.movie.value
        val trailers = viewModel.trailers.value
        val dialogQueue = viewModel.dialogQueue
        val scaffoldState = rememberScaffoldState()

        appTheme(
            isNetworkAvailable = isConnectedToInternet,
            darkTheme = isDarkTheme,
            displayProgressBar = loading.value,
            scaffoldState = scaffoldState,
            dialogQueue = dialogQueue.queue.value,
        ) {
            Scaffold(
                scaffoldState = scaffoldState,
                snackbarHost = {
                    scaffoldState.snackbarHostState
                }
            ) {
                movie?.let { m ->
                    MovieView(movie = m , trailers = trailers)
                }
            }

        }

    }
}