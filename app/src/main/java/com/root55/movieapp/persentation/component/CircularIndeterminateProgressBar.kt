package com.root55.movieapp.persentation.component

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.constraintlayout.compose.ConstraintLayout

@Composable
fun CircularIndeterminateProgressBar(
    isDisplayed: Boolean = false,
    verticalBase: Float
) {
    if(isDisplayed){
        ConstraintLayout(modifier = Modifier.fillMaxSize()) {
            val (loader) = createRefs()
            val topBias = createGuidelineFromTop(verticalBase)
            CircularProgressIndicator(modifier = Modifier.constrainAs(loader) {
                top.linkTo(topBias)
                end.linkTo(parent.end)
                start.linkTo(parent.start)
            }, color = MaterialTheme.colors.primary)
        }
    }

}