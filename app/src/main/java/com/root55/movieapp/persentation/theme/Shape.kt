package com.root55.movieapp.persentation.theme
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Shapes
import androidx.compose.ui.unit.dp

object Shape {
    val AppShapes = Shapes(
        small = RoundedCornerShape(4.dp),
        medium = RoundedCornerShape(6.dp) ,
        large = RoundedCornerShape(8.dp)
    )
}