package com.root55.movieapp.persentation.component

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.AlertDialog
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@Composable
fun GenericDialog(
    modifier: Modifier = Modifier,
    onDismiss: () -> Unit,
    title: String,
    description: String?,
    positiveAction: PositiveAction?,
    negativeAction: NegativeAction?,

    ) {
    AlertDialog(
        onDismissRequest = onDismiss,
        modifier = modifier,
        title = { Text(text = title) },
        text = {
            if (description != null)
                Text(text = description)
        },
        buttons = {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp),
                horizontalArrangement = Arrangement.End
            ) {
                if (positiveAction != null) {
                    Button(onClick = positiveAction.onClick) {
                        Text(text = positiveAction.title)
                    }
                }
                if (negativeAction != null) {
                    Button(onClick = negativeAction.onClick) {
                        Text(text = negativeAction.title)
                    }
                }
            }
        }
    )
}

data class PositiveAction(
    val onClick: () -> Unit,
    val title: String
)

data class NegativeAction(
    val onClick: () -> Unit,
    val title: String
)
