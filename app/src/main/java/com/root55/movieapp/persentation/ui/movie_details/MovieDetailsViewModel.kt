package com.root55.movieapp.persentation.ui.movie_details

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.root55.movieapp.domain.models.Movie
import com.root55.movieapp.domain.models.Trailer
import com.root55.movieapp.persentation.ui.main_screen.MainScreenStateEvent
import com.root55.movieapp.persentation.utlis.ConnectivityManager
import com.root55.movieapp.use_case.movie_details.MovieDetailsUseCase
import com.root55.movieapp.use_case.movie_details.MovieTrailerUseCase
import com.root55.movieapp.utils.DialogQueue
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Named

const val STATE_KEY_MOVIE = "movie.state.movie.key"

@HiltViewModel
class MovieDetailsViewModel
@Inject
constructor(
    private val connectivityManager: ConnectivityManager,
    private val movieDetailsUseCase: MovieDetailsUseCase,
    private val movieTrailerUseCase: MovieTrailerUseCase,
    @Named("api_key") private val apiKey: String,
    private val savedStateHandle: SavedStateHandle,
) : ViewModel() {

    val movie: MutableState<Movie?> = mutableStateOf(null)
    val trailers: MutableState<List<Trailer>> = mutableStateOf<List<Trailer>>(ArrayList())
    val loading = mutableStateOf(false)
    val onLoad = mutableStateOf(false)
    val dialogQueue = DialogQueue()

    init {
        savedStateHandle.get<Int>(STATE_KEY_MOVIE)?.let { movieId ->
            onTriggerEvent(MovieDetailsStateEvent.GetMovieDetails(movieId))
            onTriggerEvent(MovieDetailsStateEvent.GetMovieTrailers(movieId))
        }
    }

    fun onTriggerEvent(event: MovieDetailsStateEvent) {
        viewModelScope.launch {
            when (event) {
                is MovieDetailsStateEvent.GetMovieDetails -> {
                    getMovieDetails(event.movieId)
                }
                is MovieDetailsStateEvent.GetMovieTrailers -> {
                    getMovieTrailers(event.movieId)
                }
            }
        }
    }

    private fun getMovieTrailers(movieId: Int) {
        movieTrailerUseCase.execute(
            isConnectedToInternet = connectivityManager.isConnectedToInternet.value,
            apiKey = apiKey, movieId = movieId
        ).onEach { dataState ->
            loading.value = dataState.loading

            dataState.data?.let {
                trailers.value = it
            }

            dataState.error?.let{
                dialogQueue.appendErrorMessage(title = "Unknown Error!" , desc = dataState.error)
            }

        }.launchIn(viewModelScope)
    }

    private fun getMovieDetails(movieId: Int) {
        movieDetailsUseCase.execute(
            apiKey = apiKey,
            isConnectedToInternet = connectivityManager.isConnectedToInternet.value,
            movieId = movieId
        ).onEach {

            loading.value = it.loading
            it.data?.let { m ->
                movie.value = m
                savedStateHandle.set(STATE_KEY_MOVIE, m.id)
            }

            it.error?.let { error ->
                dialogQueue.appendErrorMessage("Unknown Error!!", error)
            }
        }.launchIn(viewModelScope)
    }
}