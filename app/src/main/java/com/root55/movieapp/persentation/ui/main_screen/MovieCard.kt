package com.root55.movieapp.persentation.ui.main_screen

import android.widget.RatingBar
import androidx.compose.animation.*
 import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Modifier
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.ui.tooling.preview.Preview
import com.root55.movieapp.R
import com.root55.movieapp.domain.models.TopMovie
import com.root55.movieapp.utils.loadPicture
import kotlinx.coroutines.*


const val DEFAULT_MOVIE_POSTER = R.drawable.ic_launcher_foreground
const val BASE_IMAGE_URL = "https://image.tmdb.org/t/p/original"


@Composable
fun TopMovieCard(
    movie: TopMovie ,
    onClick : () -> Unit
) {

    Card(
        shape = MaterialTheme.shapes.small,
        modifier = Modifier
//            .padding(20.dp) // margin
//            .border(2.dp, Color.Blue) // outer border
//            .padding(8.dp) // space between the borders
//            .border(2.dp, Color.Black) // inner border
            .padding(8.dp) // pa
            .width(160.dp).height(300.dp).clickable (onClick = onClick )
    ) {
        Column {
            val image = loadPicture(
                url = BASE_IMAGE_URL + movie.poster_path,
                defaultImage = DEFAULT_MOVIE_POSTER
            ).value
            image?.let {
                Image(
                    bitmap = it.asImageBitmap(),
                    contentDescription = "Poster Image",
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(225.dp),
                    contentScale = ContentScale.Crop,
                )
            }

            Column(  verticalArrangement = Arrangement.SpaceBetween) {
                Text(text = movie.original_title , style = MaterialTheme.typography.h5 , overflow = TextOverflow.Ellipsis , modifier = Modifier.width(115.dp) , maxLines = 1)
                Text(text = movie.release_date , style = MaterialTheme.typography.h5)
             }
        }
    }
}