package com.root55.movieapp.persentation.ui.main_screen

sealed class MainScreenStateEvent {
    object GetTopMovies : MainScreenStateEvent()
    object TopRatedMovies : MainScreenStateEvent()
    object Search : MainScreenStateEvent()
 }