package com.root55.movieapp.persentation.utlis

import android.util.Log
import com.root55.movieapp.utils.Constants.TAG
import java.net.InetSocketAddress
import java.net.SocketException
import javax.net.SocketFactory

object IsConnectedToInternet {
    fun isCapabilityConnectedToInternet(socketFactory: SocketFactory): Boolean {
        return try {
            val socket = socketFactory.createSocket()?:throw Exception("Socket Error")
            socket.connect(InetSocketAddress("8.8.8.8" , 53) , 1500)
            socket.close()
            Log.d(TAG, "PING success.")
            true
        } catch (error: SocketException) {
            Log.d(TAG, "isCapabilityConnectedToInternet: $error")
            false
        }
    }

}