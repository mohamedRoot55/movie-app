package com.root55.movieapp.persentation.utlis

import android.content.Context
import android.content.Context.CONNECTIVITY_SERVICE
import android.net.Network
import kotlinx.coroutines.flow.MutableStateFlow
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.NetworkCapabilities.NET_CAPABILITY_INTERNET
import android.net.NetworkRequest
import com.root55.movieapp.persentation.utlis.IsConnectedToInternet.isCapabilityConnectedToInternet
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ConnectivityFlow
constructor(application: Context) {

    private lateinit var networkCallback: ConnectivityManager.NetworkCallback

    private val cm = application.getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
    private val validNetworks: MutableSet<Network> = HashSet()

    val isConnectedToInternetStateFlow = MutableStateFlow(false)

    init {
        checkNetworkConnectivity()
    }

    private fun checkNetworkConnectivity() {
        networkCallback = createNetworkCallBack()
        val networkRequest = NetworkRequest.Builder()
            .addCapability(NET_CAPABILITY_INTERNET).build()
        cm.registerNetworkCallback(networkRequest, networkCallback)
    }

    private fun createNetworkCallBack() = object : ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network) {
            super.onAvailable(network)
            val netWorkCapability = cm.getNetworkCapabilities(network)
            val hasNetWorkCapability =
                netWorkCapability?.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
            if (hasNetWorkCapability == true) {
                CoroutineScope(Dispatchers.IO).launch {
                    val isConnectedToInternet = isCapabilityConnectedToInternet(network.socketFactory)
                    if (isConnectedToInternet) {
                        validNetworks.add(network)
                        notifyListeners()
                    }
                }
            }
        }

        override fun onLost(network: Network) {
            super.onLost(network)
            CoroutineScope(Dispatchers.IO).launch {
                validNetworks.remove(network)
                notifyListeners()
            }
        }

    }

    private suspend fun notifyListeners() {
        isConnectedToInternetStateFlow.emit(value = validNetworks.size > 0)
    }


}