package com.root55.movieapp.persentation.ui.movie_details

sealed class MovieDetailsStateEvent {
    data class GetMovieDetails(val movieId : Int) : MovieDetailsStateEvent()
    data class GetMovieTrailers(val movieId : Int) : MovieDetailsStateEvent()
}