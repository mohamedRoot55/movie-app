package com.root55.movieapp.persentation.utlis

import android.app.Application
import android.util.Log
import androidx.compose.runtime.mutableStateOf
import com.root55.movieapp.utils.Constants.TAG
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ConnectivityManager
@Inject
constructor(application: Application) {
    private val connectivityFlow = ConnectivityFlow(application = application).isConnectedToInternetStateFlow
    val isConnectedToInternet = mutableStateOf(false)

   suspend fun registerToNetworkFlowState(){
        connectivityFlow.onEach { isConnected->
            Log.d(TAG, "registerToNetworkFlowState: $isConnected")
            isConnectedToInternet.value = isConnected
        }.collect()
    }

}