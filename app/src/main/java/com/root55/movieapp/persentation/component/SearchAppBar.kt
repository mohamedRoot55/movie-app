package com.root55.movieapp.persentation.component

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.material.icons.filled.Search
import androidx.compose.runtime.*
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import com.root55.movieapp.R
import com.root55.movieapp.domain.models.TopMovie
import com.root55.movieapp.persentation.routes.MainScreenRoutes
import com.root55.movieapp.persentation.ui.main_screen.BASE_IMAGE_URL
import com.root55.movieapp.persentation.ui.main_screen.TopMovieCard
import com.root55.movieapp.utils.loadPicture


@ExperimentalAnimationApi
@ExperimentalComposeUiApi
@Composable
fun SearchAppBar(
    query: String,
    onQueryChanged: (String) -> Unit,
    onExecuteSearch: () -> Unit,
    onToggleTheme: () -> Unit,
    movies: List<TopMovie>? = null,
    onNavigate : (String) -> Unit ,
) {
    var expanded by remember { mutableStateOf(false) }
    var current_query by remember { mutableStateOf("") }
    //val focusManager = LocalFocusManager.current
    val keyboardController = LocalSoftwareKeyboardController.current
    Surface(
        modifier = Modifier
            .fillMaxWidth(),
        color = MaterialTheme.colors.secondary,
        elevation = 8.dp,
    ) {
        Column {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
            ) {
                TextField(
                    modifier = Modifier
                        .fillMaxWidth(.9f)
                        .padding(8.dp),
                    value = current_query,
                    onValueChange = { current_query = it },
                    label = { Text(text = "Search") },
                    keyboardOptions = KeyboardOptions(
                        keyboardType = KeyboardType.Text,
                        imeAction = ImeAction.Done,
                    ),
                    keyboardActions = KeyboardActions(
                        onDone = {
                            if (current_query.isNotBlank()) {
                                onQueryChanged(current_query)
                                onExecuteSearch()
                                //focusManager.clearFocus(forcedClear = true) // close keyboard
                                keyboardController?.hideSoftwareKeyboard() // another way to close keyboard
                            }
                        },
                    ),
                    leadingIcon = { Icon(Icons.Filled.Search, contentDescription = "Search Icon") },
                    textStyle = TextStyle(color = MaterialTheme.colors.onSurface),
                    colors = TextFieldDefaults.textFieldColors(backgroundColor = MaterialTheme.colors.surface),
                )
                ConstraintLayout(
                    modifier = Modifier.align(Alignment.CenterVertically)
                ) {
                    val (menu) = createRefs()
                    IconButton(
                        modifier = Modifier
                            .constrainAs(menu) {
                                end.linkTo(parent.end)
                                linkTo(top = parent.top, bottom = parent.bottom)
                            },
                        onClick = onToggleTheme,
                    ) {
                        Icon(Icons.Filled.MoreVert, contentDescription = "Toggle Dark/Light Theme")
                    }
                }
            }

            if (movies != null) {
                AnimatedVisibility(visible = movies.isNotEmpty()) {
                    LazyColumn {
                        itemsIndexed(items = movies) { index, item ->

                            val image = loadPicture(
                                url = BASE_IMAGE_URL + item.poster_path,
                                defaultImage = R.drawable.ic_launcher_foreground
                            )
                            Row(
                                modifier = Modifier
                                    .padding(8.dp)
                                    .border(width = 2.dp, color = Color.LightGray)
                                    .padding(8.dp)
                                    .fillMaxWidth().clickable {
                                        val route = MainScreenRoutes.movie_details + "/${item.id}"
                                        onNavigate(route)
                                    },
                                horizontalArrangement = Arrangement.Start,
                                verticalAlignment = Alignment.CenterVertically
                            ) {
                                image.value?.let {
                                    Image(
                                        bitmap = it.asImageBitmap(),
                                        contentDescription = "Poster Image",
                                        modifier = Modifier
                                            .width(20.dp)
                                            .height(20.dp)
                                            .background(Color.Black) , contentScale = ContentScale.Crop
                                    )
                                }

                                Text(
                                    text = item.original_title,
                                    style = MaterialTheme.typography.body1,
                                )


                            }

                        }
                    }
                }
            }
        }
    }
}