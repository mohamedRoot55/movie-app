package com.root55.movieapp.persentation.component

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@Composable
fun ConnectivityMonitor(
    isConnectedToInternet : Boolean
){
    if(!isConnectedToInternet){
      Column(modifier = Modifier.fillMaxWidth()) {
          Text(
              text = "No Internet Connection" ,
              modifier = Modifier.align(Alignment.CenterHorizontally).padding(8.dp),
          )
      }
    }

}