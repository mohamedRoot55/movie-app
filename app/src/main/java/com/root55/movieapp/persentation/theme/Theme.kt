package com.root55.movieapp.persentation.theme

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.ScaffoldState
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import com.root55.movieapp.persentation.component.*
import java.util.*


object Theme {
    private val LightThemeColors = lightColors(
        primary = Blue600,
        primaryVariant = Blue400,
        onPrimary = Black2,
        secondary = Color.White,
        secondaryVariant = Teal300,
        onSecondary = Color.Black,
        error = RedErrorDark,
        onError = RedErrorLight,
        background = Grey1,
        onBackground = Color.Black,
        surface = Color.White,
        onSurface = Black2,
    )

    private val DarkThemeColors = darkColors(
        primary = Blue700,
        primaryVariant = Color.White,
        onPrimary = Color.White,
        secondary = Black1,
        onSecondary = Color.White,
        error = RedErrorLight,
        background = Color.Black,
        onBackground = Color.White,
        surface = Black1,
        onSurface = Color.White,
    )


    @Composable
    fun appTheme(
        darkTheme: Boolean,
        isNetworkAvailable: Boolean,
        displayProgressBar: Boolean,
        scaffoldState: ScaffoldState,
        dialogQueue: Queue<GeneralDialogInfo>? = null,
        content: @Composable () -> Unit
    ) {
        MaterialTheme(
            shapes = Shape.AppShapes,
            typography = CairoTypography,
            colors = if (darkTheme) DarkThemeColors else LightThemeColors,
        ) {
            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .background(if (!darkTheme) Color.Gray else Color.Black)
            ) {
                Column {
                    ConnectivityMonitor(isNetworkAvailable)
                    content()
                }
                CircularIndeterminateProgressBar(isDisplayed = displayProgressBar, .3f)
                DefaultSnackBar(
                    snackbarHostState = scaffoldState.snackbarHostState,
                    modifier = Modifier.align(
                        Alignment.BottomCenter
                    ),
                    onDismiss = {
                        scaffoldState.snackbarHostState.currentSnackbarData?.dismiss()
                    }
                )
                ProgressDialogQueue(dialogQueue)
            }
        }

    }

}

@Composable
fun ProgressDialogQueue(dialogQueue: Queue<GeneralDialogInfo>?) {
    dialogQueue?.peek()?.let { dialogInfo ->
        GenericDialog(
            onDismiss = dialogInfo.onDismiss,
            title = dialogInfo.title,
            description = dialogInfo.description,
            positiveAction = dialogInfo.positiveAction,
            negativeAction = dialogInfo.negativeAction
        )
    }
}