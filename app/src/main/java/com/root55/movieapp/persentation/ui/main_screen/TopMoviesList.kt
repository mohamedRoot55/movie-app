package com.root55.movieapp.persentation.ui.main_screen

 import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.constraintlayout.compose.ConstraintLayout
import com.root55.movieapp.domain.models.TopMovie
 import com.root55.movieapp.persentation.routes.MainScreenRoutes.movie_details

@Composable
fun TopMoviesList(
    loading: Boolean,
    movies : List<TopMovie>,
    onTriggerEvent : ()-> Unit,
    onNavigate : (String) -> Unit ,
    //onChangeScrollPosition: (Int) -> Unit,
    page : Int
){
    Box(modifier = Modifier.background(MaterialTheme.colors.surface)){
//        if (movies.isEmpty()) {
//            ConstraintLayout(modifier = Modifier.fillMaxSize()) {
//                val(textView) = createRefs()
//                Text(text = "No Data Found....", modifier = Modifier.constrainAs(textView) {
//                    top.linkTo(parent.top)
//                    bottom.linkTo(parent.bottom)
//                    start.linkTo(parent.start)
//                    end.linkTo(parent.end)
//                })
//            }
//        }else {
//
//        }
        LazyRow{
            itemsIndexed(items = movies){ index, item ->
                //   onChangeScrollPosition(index)
                if((index + 1) > (page * 30) && !loading){
                    onTriggerEvent()
                }
                TopMovieCard(movie = item ,
                    onClick = {
                        val route = movie_details + "/${item.id}"
                        onNavigate(route)
                    }
                )
            }
        }
    }
}