package com.root55.movieapp.persentation.ui

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.platform.LocalContext
import androidx.hilt.navigation.HiltViewModelFactory
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.navArgument
import androidx.navigation.compose.rememberNavController
import com.root55.movieapp.datastore.SettingDataStore
import com.root55.movieapp.persentation.routes.MainScreenRoutes
import com.root55.movieapp.persentation.ui.main_screen.MainScreenViewModel
import com.root55.movieapp.persentation.ui.movie_details.MovieDetailsScreen
import com.root55.movieapp.persentation.ui.movie_details.MovieDetailsViewModel
import com.root55.movieapp.persentation.utlis.ConnectivityManager
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject
import androidx.navigation.compose.*

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var dataStore: SettingDataStore

    @Inject
    lateinit var connectivityManager: ConnectivityManager

    override fun onStart() {
        super.onStart()
        CoroutineScope(Dispatchers.Main).launch {
            connectivityManager.registerToNetworkFlowState()
        }
    }

    @ExperimentalAnimationApi
    @ExperimentalComposeUiApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val navController = rememberNavController()
            NavHost(
                navController = navController,
                startDestination = MainScreenRoutes.main_screen
            ) {
                composable(route = MainScreenRoutes.main_screen) { navBackStackEntry ->
                    val factory = HiltViewModelFactory(LocalContext.current, navBackStackEntry)
                    val viewModel: MainScreenViewModel = viewModel("MainScreen ViewModel", factory)
                    MainScreen(
                        isDarkTheme = dataStore.isDark.value,
                        isNetworkAvailable = connectivityManager.isConnectedToInternet.value,
                        onToggleTheme = { dataStore.toggleTheme() },
                        onNavigate = navController::navigate,
                        moviesViewModel = viewModel
                    )
                }
                composable(route = MainScreenRoutes.movie_details + "/{movieId}",
                    arguments = listOf(navArgument("movieId") {
                        type = NavType.IntType
                    })
                ) { navBackStackEntry ->
                    val factory = HiltViewModelFactory(LocalContext.current, navBackStackEntry)
                    val viewModel: MovieDetailsViewModel =
                        viewModel("MovieDetailsViewModel", factory)
                    MovieDetailsScreen(
                        isConnectedToInternet = connectivityManager.isConnectedToInternet.value,
                        isDarkTheme = dataStore.isDark.value,
                        movieId = navBackStackEntry.arguments?.getInt("movieId"),
                        viewModel = viewModel
                    )
                }
            }
        }

    }
}