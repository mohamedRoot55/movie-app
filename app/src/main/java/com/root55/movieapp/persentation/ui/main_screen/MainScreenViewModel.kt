package com.root55.movieapp.persentation.ui.main_screen

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.SavedStateHandle
import com.root55.movieapp.persentation.utlis.ConnectivityManager
import com.root55.movieapp.use_case.main_screen.PopularMoviesUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import javax.inject.Named
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.root55.movieapp.domain.models.TopMovie
import com.root55.movieapp.use_case.main_screen.SearchForMoviesUseCase
import com.root55.movieapp.use_case.main_screen.TopRatedMoviesUseCase
import com.root55.movieapp.use_case.movie_details.MovieDetailsUseCase
import com.root55.movieapp.utils.DialogQueue
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

const val STATE_KEY_QUERY = "movies.state.query.key"

@HiltViewModel
class MainScreenViewModel
@Inject
constructor(
    private val connectivityManager: ConnectivityManager,
    private val popularMoviesUseCase: PopularMoviesUseCase,
    private val topRatedMoviesUseCase: TopRatedMoviesUseCase,
    private val searchForMovies: SearchForMoviesUseCase,
    @Named("api_key") private val apiKey: String,
    private val savedStateHandle: SavedStateHandle,
) : ViewModel() {

    val query = mutableStateOf("")
    val page = mutableStateOf(1)
    val loading = mutableStateOf(false)
    val popular_movies = mutableStateOf<List<TopMovie>>(ArrayList())
    val query_movies = mutableStateOf<List<TopMovie>>(ArrayList())
    val top_rated_movies = mutableStateOf<List<TopMovie>>(ArrayList())
    val dialogQueue = DialogQueue()

    init {
        savedStateHandle.get<String>(STATE_KEY_QUERY)?.let { p ->
            setQuery(query = p)
        }
        onTriggerEvent(MainScreenStateEvent.GetTopMovies)
        onTriggerEvent(MainScreenStateEvent.TopRatedMovies)
    }

    fun onQueryChanged(query: String) {
        setQuery(query)
    }


    private fun setQuery(query: String) {
        this.query.value = query
        savedStateHandle.set(STATE_KEY_QUERY, query)
    }

    fun onTriggerEvent(event: MainScreenStateEvent) {
        viewModelScope.launch {
            when (event) {
                is MainScreenStateEvent.GetTopMovies -> {
                    getTopMovies()
                }
                is MainScreenStateEvent.TopRatedMovies -> {
                    getTopRatedMovies()
                }
                is MainScreenStateEvent.Search -> {
                    searchForMovies()
                }

            }
        }
    }


    fun searchForMovies() {
        searchForMovies.execute(query = query.value).onEach {
            loading.value = it.loading
            it.data?.let { movies ->
                query_movies.value = movies
            }

            it.error?.let { error ->
                dialogQueue.appendErrorMessage(title = "Unknown Error!", desc = error)
            }
        }.launchIn(viewModelScope)
    }

    private fun getTopRatedMovies() {
        topRatedMoviesUseCase.execute(
            connectivityManager.isConnectedToInternet.value,
            apiKey = apiKey,
            page = page.value
        ).onEach { dataState ->
            loading.value = dataState.loading

            dataState.data?.let {
                top_rated_movies.value = it
            }

            dataState.error?.let {
                dialogQueue.appendErrorMessage(desc = "Unknown Error", title = "Error!")
            }
        }.launchIn(viewModelScope)
    }

    fun getTopMovies() {
        popularMoviesUseCase.execute(
            apiKey = apiKey,
            isConnectedToInternet = connectivityManager.isConnectedToInternet.value,
            page = page.value
        ).onEach { dataState ->
            loading.value = dataState.loading

            dataState.data?.let {
                popular_movies.value = it
            }

            dataState.error?.let {
                dialogQueue.appendErrorMessage(
                    desc = dataState.error,
                    title = "ERROR OCCUR"
                )
            }

        }.launchIn(viewModelScope)
    }
}