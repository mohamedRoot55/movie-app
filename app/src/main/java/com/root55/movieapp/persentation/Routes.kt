package com.root55.movieapp.persentation

object Routes {
    const val main_screen = "MAIN_SCREEN"
    const val movie_details = "MOVIE_DETAILS"
    const val see_all_top_popular_movies = "SEE_ALL_TOP_POPULAR_MOVIES"
}