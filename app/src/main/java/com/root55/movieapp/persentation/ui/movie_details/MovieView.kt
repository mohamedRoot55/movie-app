package com.root55.movieapp.persentation.ui.movie_details

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.root55.movieapp.R
import com.root55.movieapp.domain.models.Movie
import com.root55.movieapp.domain.models.Trailer
import com.root55.movieapp.persentation.ui.main_screen.BASE_IMAGE_URL
import com.root55.movieapp.persentation.ui.main_screen.DEFAULT_MOVIE_POSTER
import com.root55.movieapp.utils.loadPicture


@Composable
fun MovieView(
    movie: Movie,
    trailers: List<Trailer>? = null
) {
    LazyColumn(
        modifier = Modifier
            .fillMaxSize()
    ) {
        item {
            val movieImage = loadPicture(
                url = BASE_IMAGE_URL + movie.poster_path,
                defaultImage = R.drawable.ic_launcher_foreground
            )
            movieImage.value?.let {
                Image(
                    bitmap = it.asImageBitmap(),
                    contentDescription = "Poster Image",
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(220.dp),
                    contentScale = ContentScale.Crop,
                )
            }
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(1.dp)
                    .padding(horizontal = 6.dp)
                    .background(Color.Gray)
            )
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .background(MaterialTheme.colors.secondary),
                horizontalArrangement = Arrangement.SpaceAround
            ) {
                Column(horizontalAlignment = Alignment.CenterHorizontally) {
                    Text(text = movie.popularity.toString(), style = MaterialTheme.typography.h5)
                    Text(text = "popularity", style = MaterialTheme.typography.h5)
                }
                Column(horizontalAlignment = Alignment.CenterHorizontally) {
                    Image(
                        painter = painterResource(R.drawable.star),
                        contentDescription = "Star",
                        modifier = Modifier
                            .width(20.dp)
                            .height(20.dp)
                    )
                    Text(text = movie.vote_average.toString(), style = MaterialTheme.typography.h5)
                }
                Column(horizontalAlignment = Alignment.CenterHorizontally) {
                    Text(text = movie.vote_count.toString())
                    Text(text = "Vote Count")
                }

            }
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(1.dp)
                    .padding(horizontal = 6.dp)
                    .background(Color.Gray)
            )
            Text(text = "Over View", style = MaterialTheme.typography.h2)
            Text(text = movie.overview)

            Text(text = "Production Company", style = MaterialTheme.typography.h2)


        }
        item {
            LazyRow {
                itemsIndexed(items = movie.production_companies) { index, item ->

                    val companyImage = loadPicture(
                        url = BASE_IMAGE_URL + item.logo_path,
                        defaultImage = DEFAULT_MOVIE_POSTER
                    )
                    Row(verticalAlignment = Alignment.CenterVertically,
                        modifier = Modifier.background(Color.Gray).fillMaxWidth()
                            .padding(16.dp)
                            .clip(MaterialTheme.shapes.large)


                    ) {
                        companyImage.value?.let {
                            Image(
                                bitmap = it.asImageBitmap(),
                                contentDescription = "Poster Image",
                                modifier = Modifier
                                    .width(150.dp)
                                    .height(150.dp).padding(16.dp).clip(MaterialTheme.shapes.medium) , contentScale = ContentScale.Crop
                            )

                        }
                    }
                }
            }
        }
    }


}