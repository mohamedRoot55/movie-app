package com.root55.movieapp.persentation.ui

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.gestures.ScrollableState
import androidx.compose.foundation.gestures.scrollable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.root55.movieapp.persentation.component.SearchAppBar
import com.root55.movieapp.persentation.theme.Theme.appTheme
import com.root55.movieapp.persentation.ui.main_screen.MainScreenStateEvent
import com.root55.movieapp.persentation.ui.main_screen.MainScreenViewModel
import com.root55.movieapp.persentation.ui.main_screen.TopMoviesList
import androidx.navigation.compose.*

@ExperimentalAnimationApi
@ExperimentalComposeUiApi
@Composable
fun MainScreen(
    isDarkTheme: Boolean,
    isNetworkAvailable: Boolean,
    onToggleTheme: () -> Unit,
    onNavigate: (String) -> Unit,
    moviesViewModel: MainScreenViewModel
) {
    val query = moviesViewModel.query.value
    val popular = moviesViewModel.popular_movies
    val top_movies = moviesViewModel.top_rated_movies
    val loading = moviesViewModel.loading
    val page = moviesViewModel.page
    val dialogQueue = moviesViewModel.dialogQueue
    val scaffoldState = rememberScaffoldState()
    appTheme(
        displayProgressBar = loading.value,
        darkTheme = isDarkTheme,
        isNetworkAvailable = isNetworkAvailable,
        scaffoldState = scaffoldState,
        dialogQueue = dialogQueue.queue.value,
        content = {
            Scaffold(
                topBar = {
                    SearchAppBar(
                        query = query,
                        onQueryChanged = moviesViewModel::onQueryChanged ,
                        onToggleTheme =  {onToggleTheme()} ,
                        onExecuteSearch = moviesViewModel::searchForMovies,
                        movies = moviesViewModel.query_movies.value,
                        onNavigate = onNavigate
                    )
                },
                scaffoldState = scaffoldState,
                snackbarHost = {
                    scaffoldState.snackbarHostState
                }
            ) {

                Column(
                    modifier = Modifier
                        .fillMaxHeight()
                        .verticalScroll(rememberScrollState()),
                    horizontalAlignment = Alignment.Start
                ) {
                    Text(
                        text = "Popular Movies",
                        modifier = Modifier.padding(16.dp),
                        style = MaterialTheme.typography.body1
                    )
                    TopMoviesList(
                        loading = loading.value,
                        movies = popular.value,
                        onTriggerEvent = { moviesViewModel.onTriggerEvent(MainScreenStateEvent.GetTopMovies) },
                        page = page.value,
                        onNavigate = onNavigate
                        //onChangeScrollPosition = {moviesViewModel.onChangeScroll}
                    )
                    Text(
                        text = "Top Movies",
                        modifier = Modifier.padding(16.dp),
                        style = MaterialTheme.typography.body1
                    )
                    TopMoviesList(
                        loading = loading.value,
                        movies = top_movies.value,
                        onTriggerEvent = { moviesViewModel.onTriggerEvent(MainScreenStateEvent.TopRatedMovies) },
                        page = page.value,
                        onNavigate = onNavigate
                        //onChangeScrollPosition = {moviesViewModel.onChangeScroll}
                    )
                }
            }
        }
    )
}