package com.root55.movieapp.datastore

import androidx.compose.runtime.mutableStateOf
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.createDataStore
import com.root55.movieapp.persentation.BaseApplication
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SettingDataStore
@Inject
constructor(
  val application: BaseApplication
) {
  private var dataStore: DataStore<Preferences> = application.createDataStore(name = "setting")
  private val scope = CoroutineScope(Main)
  val isDark  = mutableStateOf(false)

  init {
    observeDataStore()
  }

  fun toggleTheme(){
    scope.launch {
      dataStore.edit {preferences ->
        val current = preferences[DARK_THEME_KEY] ?: false
        preferences[DARK_THEME_KEY] = !current
      }
    }
  }

  private fun observeDataStore(){
    dataStore.data.onEach {
      preferences ->
      preferences[DARK_THEME_KEY]?.let {
        isDarkTheme ->
        isDark.value = isDarkTheme
      }
    }.launchIn(scope = scope)
  }
  companion object {
     val DARK_THEME_KEY = booleanPreferencesKey("DARK_THEME_KEY") ;
  }

}