package com.root55.movieapp.di

import android.content.Context
import com.root55.movieapp.persentation.BaseApplication
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {
    @Provides
    @Singleton
    fun provideBaseApplication(@ApplicationContext context: Context) : BaseApplication {
        return context as BaseApplication
    }
}