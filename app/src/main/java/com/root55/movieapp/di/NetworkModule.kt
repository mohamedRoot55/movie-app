package com.root55.movieapp.di

import com.root55.movieapp.network.MoviesService
import com.root55.movieapp.utils.Constants
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {
    @Singleton
    @Provides
    fun provideRetrofitBuilder(): MoviesService {
        return Retrofit.Builder().baseUrl(Constants.BASE_URL).
        addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .build().create(MoviesService::class.java)
    }

//    @Provides
//    @Singleton
//    fun provideMoviesService(builder : Retrofit.Builder) : MoviesService{
//        return builder.build().create(MoviesService::class.java)
//    }


    @Provides
    @Singleton
    @Named("api_key")
    fun provideApiKey() : String {
        return Constants.API_KEY
    }
}