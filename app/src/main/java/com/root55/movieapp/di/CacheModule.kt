package com.root55.movieapp.di

import androidx.room.Room
import com.root55.movieapp.chache.dao.MovieDao
import com.root55.movieapp.chache.dao.TopMovieDao
import com.root55.movieapp.chache.database.AppDatabase
import com.root55.movieapp.chache.database.AppDatabase.Companion.APP_DATABASE_NAME
import com.root55.movieapp.persentation.BaseApplication
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object CacheModule {

    @Provides
    @Singleton
    fun provideAppDataBase(application: BaseApplication): AppDatabase {
        return Room.databaseBuilder(application, AppDatabase::class.java, APP_DATABASE_NAME).build()
    }

    @Provides
    @Singleton
    fun provideTopPopularMovieDao(appDatabase: AppDatabase) : TopMovieDao {
        return appDatabase.topMovieDao() ;
    }

    @Provides
    @Singleton
    fun provideMovieDao(appDatabase: AppDatabase) : MovieDao {
        return appDatabase.movieDao()
    }


}