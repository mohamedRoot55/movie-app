package com.root55.movieapp.di

import com.root55.movieapp.chache.dao.MovieDao
import com.root55.movieapp.chache.dao.TopMovieDao
import com.root55.movieapp.network.MoviesService
import com.root55.movieapp.use_case.main_screen.PopularMoviesUseCase
import com.root55.movieapp.use_case.main_screen.SearchForMoviesUseCase
import com.root55.movieapp.use_case.main_screen.TopRatedMoviesUseCase
import com.root55.movieapp.use_case.movie_details.MovieDetailsUseCase
import com.root55.movieapp.use_case.movie_details.MovieTrailerUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object UseCaseModule {

    @Provides
    @Singleton
    fun providePopularMoviesUseCase(
        topMovieDao: TopMovieDao,
        moviesService: MoviesService
    ): PopularMoviesUseCase {
        return PopularMoviesUseCase(
            topMovieDao,
            moviesService
        )
    }


    @Provides
    @Singleton
    fun provideTopRatedMoviesUseCase(
        topMovieDao: TopMovieDao,
        moviesService: MoviesService
    ): TopRatedMoviesUseCase {
        return TopRatedMoviesUseCase(
            topMovieDao,
            moviesService
        )
    }

    @Singleton
    @Provides
    fun provideSearchForMoviesUseCase(topMovieDao: TopMovieDao): SearchForMoviesUseCase {
        return SearchForMoviesUseCase(topMovieDao = topMovieDao)
    }

    @Singleton
    @Provides
    fun provideGetMovieDetailsUseCase(
        movieDao : MovieDao ,
        moviesService: MoviesService
    ) : MovieDetailsUseCase {
        return MovieDetailsUseCase(movieDao = movieDao , moviesService = moviesService)
    }

    @Provides
    @Singleton
    fun provideTrailerMoviesUseCase(moviesService: MoviesService) : MovieTrailerUseCase{
        return  MovieTrailerUseCase(moviesService = moviesService)
    }
}